16:22 < sphericon>                 $data = array(
16:22 < sphericon>                  "username" => $this->username,
16:22 < sphericon>                   "password" => $this->password,
16:22 < sphericon>                   "deviceID" => $this->device_id,
16:22 < sphericon>                 );
16:23 < sphericon> $response = $this->get_curl_response('/api/login', $data_encoded);
16:23 < sphericon> that gives a token


16:06 < sphericon> You can call it via url: http://cryptokingdom.me/api/characterInfo/[token] <---woohooo!!!
16:06 < moneromooo> Oh, that sounds good, I'll try it :)
16:17 < Syksy> sphericon: wha huh new api stuff?!
16:17 < sphericon> {"status":200,"error":false,"data":{"characterID":"207","fullName":null,"shortName":"Cooper","title":null,"level":null,"born":"1606","age":"18","died":"0","balance":140686929}}
16:17 < sphericon> yep!
16:17 < Syksy> awww yeeeee

16:25 < sphericon> https://github.com/saddammonero/cryptokingdom
16:25 < sphericon> https://github.com/spherico/ck_api

16:25 < sphericon> http://www.omegahat.org/RCurl/installed/RCurl/html/getURL.html
16:26 < sphericon> never mind, that is a blank page
16:26 < sphericon> mytxt <- getURL("http://rxnav.nlm.nih.gov/REST/rxcui/866350/allrelated", httpheader=c(Accept="text/plain"))
16:26 < moneromooo> At least there's no misleading info on it :P
16:26 < sphericon> apparently that works in r


16:27 < sphericon>  $response = $this->get_curl_response('/api/login', $data_encoded); $token = $response->data->token;

16:28 < sphericon> what I do is store the key in the db along with a hash value that is unique to char
16:29 < sphericon> the hash is written to a cookie

16:29 < moneromooo> http://cryptokingdom.me/api/characterInfo/ gives me 404
16:29 < moneromooo> http://cryptokingdom.me/api/characterInfo/[token] is probably not a corect IRU


16:29 < moneromooo> http://cryptokingdom.me/api/characterInfo/ gives me 404
16:29 < moneromooo> http://cryptokingdom.me/api/characterInfo/[token] is probably not a corect IRU
16:29 < moneromooo> token is usually passed in the data, not the URI
16:29 < Syksy> hmm alrighty
16:29 < Syksy> I wish these were documented by PJ
16:30 < sphericon> and it is used in subsequent requests to the API for info. The app then gets the CK_token from my db using the hash as a lookup, then makes the
                   request.
16:30 < Syksy> would make things so much more fluent
16:30 < moneromooo> sphericon got it to work, but just won't post an actual URI :D
16:30 < sphericon> moneromooo: the fact that token isn't in a data array could be problematic
16:30 < sphericon> but it does work for now.

16:31 < Syksy> tell the dumb young padawan, what is difference between a key and a token :P
16:31 < moneromooo> I have a token when I login.
16:32 < sphericon> syksy: it's just the wrong name for what we're looking for. in an array, the key could be  a number
16:32 < Syksy> hm okay
16:32 < sphericon> $chars[0]  == 'HM_The_King"
16:32 < sphericon> key = 0
16:32 < sphericon> value = "HM_The_King"
