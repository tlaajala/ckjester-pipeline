setwd("D:\\CKJester\\DB")

files <- list.files()
# Only take parsed item files, not raw API-input
files <- files[grep("offersParsed", files)]
# Allocate an empty list to collect parsed files to
collect <- vector("list", length=length(files))
# Loop through available readily parsed files
for(i in 1:length(files)){
	load(files[i])
	collect[[i]] <- dat
}
# Perform row-binding
db <- do.call("rbind", collect)
db[,"price"] <- as.numeric(db[,"price"])
db[,"amount"] <- as.numeric(db[,"amount"])
db[,"characterID"] <- as.numeric(db[,"characterID"])
db[,"bid"] <- as.numeric(db[,"bid"])

save(db, file=paste("DB_", Sys.Date(), ".RData", sep=""))

